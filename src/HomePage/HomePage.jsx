import React from 'react';
import { Switch, BrowserRouter, Route, Link, withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';
import config from 'config';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import styles from './styles'
import sidebarRoutes from '../_helpers/sidebarRoutes'

// TOREM
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    background: config.themeColor
  },
  content: {
    flexGrow: 1,
    marginTop: 60,
    padding: theme.spacing(3),
  },
  icon: {
    color: config.themeColor,
  }
}));

export const HomePage = withRouter((props) => {
  const classes = styles;
  const classes2 = useStyles();
  const { user } = useSelector(state => state.authentication);

  const drawer = (
    <div>
      <List>
        {sidebarRoutes.map((prop, key) => {
          return (
            <ListItem button key={key} component={Link} to={prop.path}>
              <ListItemIcon className={classes2.icon}>{prop.icon}</ListItemIcon>
              <ListItemText primary={prop.sidebarName} />
            </ListItem>
          )
        })}
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes2.appBar}>
        <Toolbar>
          <h3>GG 4 Tain</h3>
        </Toolbar>
      </AppBar>
      <BrowserRouter>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <Toolbar />
          <div className={classes.drawerContainer}>
            {drawer}
          </div>
        </Drawer>
        <main className={classes2.content}>
          <div className={classes.toolbar} />
          <Switch>
            {sidebarRoutes.map((route) => (
              <Route exact path={route.path} key={route.path}>
                <route.component />
              </Route>
            ))}
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
})